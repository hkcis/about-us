# About Us

We are a group of Hong Kong citizens who love justice and care about lives. We are concerned by more and more tragedies happened around us.

As we realised that most of Hong Kong citizens have lost their trust in HKSAR's police force due to their abuse of power, we feel the urge to take the rights of investigation back to the members of public.

Our primary aim is to investigate and publish. How our publications will be used is beyond our control.

We encourage Hong Kong citizens to actively participate in the investigations of various mysterious cases involving grievous bodily harm, cause of death, abuse of power etc. Every smallest clue you will provide has its part in solving the mystery.
